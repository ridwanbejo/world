<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cretosphere - World Database</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendors/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendors/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendors/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper" ng-app="countryApp">
        <div ng-include="'partials/template-nav'"></div>
        <div id="page-wrapper">
            <div ng-view></div>
        </div>
    </div>

    
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/metisMenu/dist/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="vendors/angular/angular.min.js"></script>
    <script src="vendors/angular/angular-route.min.js"></script>
    <script src="vendors/angular/angular-resource.min.js"></script>
    <script src="vendors/angular/angular-sanitize.min.js"></script>
    <script type="text/javascript">
        var countryApp = angular.module('countryApp', ['ngRoute', 'ngResource', 'ngSanitize']);
        var base_url = 'http://localhost/world/api/public/';
        countryApp.config(['$routeProvider', function($routeProvider){
            $routeProvider.
                        when('/', {
                            templateUrl:'partials/country-list',
                            controller:'CountryListController',
                        }).
                        when('/new', {
                            templateUrl:'partials/country-edit',
                            controller:'CountryAddController',
                        }).
                        when('/:countryId', {
                            templateUrl:'partials/country-detail',
                            controller:'CountryDetailController',
                        }).
                        when('/edit/:countryId', {
                            templateUrl:'partials/country-edit',
                            controller:'CountryEditController',
                        }).
                        otherwise({
                            redirectTo: '/'
                        });
        }]);

        countryApp.directive('akModal', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.akModal, function(value) {
                        if (value) element.modal('show');
                        else element.modal('hide');
                    });
                }
            };
        });

        countryApp.directive('simpleLoading', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.simpleLoading, function(value) {
                        if (value) element.show();
                        else element.hide();
                    });
                }
            };
        });

        countryApp.directive('dynamic', function ($compile) {
          return {
            restrict: 'A',
            replace: true,
            link: function (scope, ele, attrs) {
              scope.$watch(attrs.dynamic, function(html) {
                ele.html(html);
                $compile(ele.contents())(scope);
              });
            }
          };
        });

        countryApp.directive('regionAutocomplete', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.autocomplete({
                        source:function (request, response){
                            $http.get(base_url+'country/info/region?q='+scope.country.Region).success(function(data){
                                result = [];
                                for (var i = 0; i < data.length; i++){
                                    result.push({'label':data[i].Region, 'value':data[i].Region});
                                }
                                response(result);
                            });
                        },
                    });
                }
            };
        });

         countryApp.directive('continentAutocomplete', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.autocomplete({
                        source:function (request, response){
                            $http.get(base_url+'country/info/continent?q='+scope.country.Continent).success(function(data){
                                result = [];
                                for (var i = 0; i < data.length; i++){
                                    result.push({'label':data[i].Continent, 'value':data[i].Continent});
                                }
                                response(result);
                            });
                        },
                    });
                }
            };
        });

         countryApp.directive('govtAutocomplete', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.autocomplete({
                        source:function (request, response){
                            $http.get(base_url+'country/info/governmentForm?q='+scope.country.GovernmentForm).success(function(data){
                                result = [];
                                for (var i = 0; i < data.length; i++){
                                    result.push({'label':data[i].GovernmentForm, 'value':data[i].GovernmentForm});
                                }
                                response(result);
                            });
                        },
                    });
                }
            };
        });

        countryApp.filter('spaceless',function(){
            return function(input){
                return input.replace(/\s+/g,'_');
            }
        });

        countryApp.controller('CountryListController', function($scope, $http){
            $scope.titles = "List of Country";
            $scope.showLoading = true;
            $scope.currentPage = 1;
            $scope.totalPage = 1;
            $scope.limitPage = 10;
            
            $http.get(base_url+'country?page='+$scope.currentPage+'&limit='+$scope.limitPage).success(function(data, status){
                $scope.showLoading = false;
                
                $scope.countries = data.result;
                $scope.totalPage = data.total_page;
                $scope.pager = createPager(1, $scope.totalPage);
            });

            $scope.searchCountry = function(){
                $http.get(base_url+'country?q='+$scope.q).success(function(data, status){
                    $scope.countries = data.result;
                });
                $scope.currentPage = 1;
            }

            $scope.changePage = function(page){
                $scope.showLoading = true;
                $http.get(base_url+'country?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.countries = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });
                $scope.currentPage = page;
            }

            $scope.prevPage = function(){
                if ($scope.currentPage > 1)
                {
                    var page = $scope.currentPage - 1;
                }
                else
                {
                    var page = 1;
                }
                
                console.log(page);

                $scope.showLoading = true;
                $http.get(base_url+'country?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.countries = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });
                $scope.currentPage = page;
            }

            $scope.nextPage = function(){
                if ($scope.currentPage < $scope.totalPage)
                {
                    var page = $scope.currentPage + 1;
                }
                else
                {
                    var page = $scope.totalPage;
                }

                $scope.showLoading = true;
                $http.get(base_url+'country?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.countries = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });

                $scope.currentPage = page;
            }

            function createPager(page, total)
            {
                console.log(page);
                        
                var pager_length = 5;
                var pager = "";
                if (page >= pager_length)
                {
                    if (page > total - pager_length && page <= total)
                    {
                        console.log(' page akhir ');
                        pager = '<ul class="pagination">'+
                                   '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                        
                        for (var i = total - pager_length  + 1; i <= total; i ++)
                        {
                          pager += '<li><a href="javascript:void(0);" ng-click="changePage('+ ( i ) +');">'+ ( i ) +'</a></li>';
                        }          
                            
                        if (page < total)
                        {
                            pager +=   '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>';
                        }
                        
                        pager +=   '</ul>';
                    }
                    else
                    {
                        console.log('page tengah');

                        pager = '<ul class="pagination">'+
                                   '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                        
                        for (var i = 0; i < 5; i ++)
                        {
                          pager += '<li><a href="javascript:void(0);" ng-click="changePage('+ (page + i ) +');">'+ (page + i ) +'</a></li>';
                        }          
                            
                        pager +=   '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>'+
                                '</ul>';
                    }
                    
                }
                else if (page < pager_length)
                {
                    console.log('page awal');
                    var prev ="";
                    if (page > 1)
                    {
                        prev = '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                    
                    }
                   
                    pager = '<ul class="pagination">'+
                              prev+
                              '<li><a href="javascript:void(0);" ng-click="changePage(1);">1</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(2);">2</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(3);">3</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(4);">4</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(5);">5</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>'+
                            '</ul>';
                }
                

                return pager;
            }
        });

        countryApp.controller('CountryDetailController', function($scope, $http, $routeParams){
            $scope.titles = "This is a country detail";
            $scope.showModal = false;
            $scope.showLoading = true;
            $scope.city = {name:"", district:"", population:""};
            $scope.language = {lang:"", percent:""};
            
            $http.get(base_url+'country/'+$routeParams.countryId).success(function(data, status){
                $scope.country = data;
                $http.get(base_url+'city/'+data.Capital).success(function(data, status){
                    $scope.capital = data;
                });
                $http.get(base_url+'country_lang/'+data.Code+'/country?sort_by=Percentage').success(function(data, status){
                    $scope.country_lang = data;
                });
                $http.get(base_url+'city/'+data.Code+'/country').success(function(data, status){
                    $scope.cities = data;
                });
        
                setTimeout(function(){
                    console.log('set timeout...');
                    $scope.showLoading = false;
                }, 10);
            });

            $scope.setForm = function(form_name){
                console.log(form_name);
                if (form_name == 'lang')
                {
                    $scope.modalTitle = "Add Language";
                    $scope.modalContent =   '<div class="form-group">'+
                                                '<label>Language</label>'+
                                                '<input class="form-control" value="{{language.lang}}" ng-model="language.lang">'+
                                            '</div>'+
                                            '<div class="form-group">'+
                                                '<label>Percentage</label>'+
                                                '<input class="form-control" value="{{language.percent}}" ng-model="language.percent">'+
                                            '</div>'+
                                            '<div class="form-group">'+
                                                '<label>Is Official?</label>'+
                                                '<select class="form-control" ng-model="language.is_official">'+
                                                    '<option value="T">Yes</option>'+
                                                    '<option value="F">Unofficial</option>'+
                                                '</select>'+
                                            '</div>';
                    $scope.modalType = 'lang';
                    $scope.modalAction = 'add';
                }
                else if (form_name == 'city')
                {
                    $scope.modalTitle = "Add City";
                    $scope.modalContent = '<div class="form-group">'+
                                                '<label>Name</label>'+
                                                '<input class="form-control" value="{{ city.name }}" ng-model="city.name">'+
                                            '</div>'+
                                            '<div class="form-group">'+
                                                '<label>District</label>'+
                                                '<input class="form-control" value="{{ city.district }}" ng-model="city.district">'+
                                            '</div>'+
                                            '<div class="form-group">'+
                                                '<label>Population</label>'+
                                                '<input class="form-control" value="{{ city.population }}" ng-model="city.population">'+
                                            '</div>';
                    $scope.modalType = 'city';
                    $scope.modalAction = 'add';
                }
            };

            $scope.submitForm = function(type, action){
                $scope.showModal=false;
                console.log(type);
                if (type == 'lang')
                {
                    $scope.submitLanguage(action);
                }
                else if (type == 'city')
                {
                    $scope.submitCity(action);
                }
            };

            $scope.submitLanguage = function(action){
                if (action == "add")
                {
                    $http({
                        method:"POST",
                        url: base_url+'country_lang', 
                        data: $.param({
                            'name':$scope.language.lang,
                            'country_code':$scope.country.Code,
                            'is_official':$scope.language.is_official,
                            'percentage':$scope.language.percent
                        }),
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).success(function(data, status){
                        $scope.country_lang.push({'CountryCode':$scope.country.Code, 'Language':$scope.language.lang, 'IsOfficial':$scope.language.is_official, 'Percentage':$scope.language.percent});
                    });
                }
                else if (action == "edit")
                {
                    $http({
                        method:"PUT",
                        url: base_url+'country_lang/'+$scope.country.Code+"/"+$scope.language.lang, 
                        data: $.param({
                            'is_official':$scope.language.is_official,
                            'percentage':$scope.language.percent
                        }),
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).success(function(data, status){
                    });
                }
               
            };

            $scope.submitCity = function(action){
                if (action=="add")
                {
                    $http({
                        method:"POST",
                        url: base_url+'city', 
                        data: $.param({
                            'name':$scope.city.name,
                            'country_code':$scope.country.Code,
                            'district':$scope.city.district,
                            'population':$scope.city.population
                        }),
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).success(function(data, status){
                        $scope.cities.push({'CountryCode':$scope.country.Code, 'Name':$scope.city.name, 'District':$scope.city.district, 'Population':$scope.city.population});
                    });    
                }
                else if (action == "edit")
                {
                    console.log('put city');
                    $http({
                        method:"PUT",
                        url: base_url+'city/'+$scope.city.ID, 
                        data: $.param({
                            'name':$scope.city.name,
                            'country_code':$scope.country.Code,
                            'district':$scope.city.district,
                            'population':$scope.city.population
                        }),
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).success(function(data, status){
                    });
                }
            };

            $scope.deleteCity = function(id, index){
                var delete_this = confirm('Are you sure want to delete this city?');
                if (delete_this) {
                    
                    $http.delete(base_url+'city/'+id).success(function(data, status){
                        $scope.cities.splice(index, 1);
                    });

                    window.document.location = '#/'+$scope.country.Code;
                }
            };

            $scope.deleteLanguage = function(code, lang, index){
                var delete_this = confirm('Are you sure want to delete this language?');
                if (delete_this) {
                    
                    $http.delete(base_url+'country_lang/'+code+"/"+lang).success(function(data, status){
                        $scope.country_lang.splice(index, 1);
                    });

                    window.document.location = '#/'+$scope.country.Code;
                }
            };

            $scope.editCity = function(id) {
                $scope.showModal=true;

                $http.get(base_url+'city/'+id).success(function(data, status){
                    $scope.city.ID = data.ID;
                    $scope.city.name = data.Name;
                    $scope.city.district = data.District;
                    $scope.city.population = data.Population;
                    $scope.setForm('city');
                    $scope.modalTitle = "Edit City - "+data.Name;
                    $scope.modalAction = 'edit';
                });
            };

            $scope.addCity = function(id) {
                $scope.showModal=true;
                $scope.city = {name:"", district:"", population:""};
                $scope.setForm('city');
                $scope.modalTitle = "Add City";
                $scope.modalAction = 'add';
            };

             $scope.addLanguage = function(id) {
                $scope.showModal=true;
                $scope.language = {lang:"", percent:""};
                $scope.setForm('lang');
                $scope.modalTitle = "Add Languages";
                $scope.modalAction = 'add';
            };

            $scope.editLanguage = function(id, lang) {
                $scope.showModal=true;

                $http.get(base_url+'country_lang/'+id+"/"+lang).success(function(data, status){
                    $scope.language = {lang:data.Language, percent:data.Percentage};
                    $scope.setForm('lang');
                    $scope.modalTitle = "Edit Language";
                    $scope.modalAction = 'edit';
                });
            };

            $scope.setCapital = function(id) {
                $scope.showLoading=true;
                $http({
                    method:"POST",
                    url: base_url+'country/capital', 
                    data: $.param({ 'city_id':id, 'Code':$scope.country.Code }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function(data, status){
                    $scope.showLoading = false;
                });
            };

             $scope.deleteCountry = function(code){
                var delete_this = confirm('Are you sure want to delete this country?');
                if (delete_this) {
                    
                    $http.delete(base_url+'country/'+code).success(function(data, status){
                        window.document.location = '#/';
                    });
                }
            };
        });

        countryApp.controller('CountryEditController', function($scope, $http, $routeParams){
            $http.get(base_url+'country/'+$routeParams.countryId).success(function(data, status){
                $scope.country = data;
                $scope.titles = "Edit - "+data.Name;
            });

            $scope.submitForm = function () {

                $http({
                    method:"PUT",
                    url: base_url+'country/'+$scope.country.Code, 
                    data: $.param($scope.country),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function(data, status){
                    window.document.location = "country#/"+$scope.country.Code;
                });
            }
        });

        countryApp.controller('CountryAddController', function($scope, $http){
            $scope.titles = "Add Country";
            $scope.country = {
                                "Name":"",
                                "Region":"",
                                "Continent":"",
                                "Population":"",
                                "GNP":"",
                                "SurfaceArea":"",
                                "LifeExpectancy":"",
                                "HeadOfState":"",
                                "LocalName":"",
                                "IndepYear":"",
                                "GovernmentForm":"",
                                "Code":"",
                                "Code2":""
                            };

            $scope.submitForm = function () {
                $http({
                    method:"POST",
                    url: base_url+'country', 
                    data: $.param($scope.country),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function(data, status){

                    $scope.country = {
                                "Name":"",
                                "Region":"",
                                "Continent":"",
                                "Population":"",
                                "GNP":"",
                                "SurfaceArea":"",
                                "LifeExpectancy":"",
                                "HeadOfState":"",
                                "LocalName":"",
                                "IndepYear":"",
                                "GovernmentForm":"",
                                "Code":"",
                                "Code2":""
                            };
                    window.document.location = "country#/";
                });
            }
        });
    </script>
</body>

</html>
