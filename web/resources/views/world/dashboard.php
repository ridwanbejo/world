<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cretosphere - World Database</title>
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="vendors/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper" ng-app="dashboardApp">
        <div ng-include="'partials/template-nav'"></div>
        <div id="page-wrapper">
            <div ng-view></div>
        </div>
    </div>
    
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/metisMenu/dist/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="vendors/flot/excanvas.min.js"></script>
    <script src="vendors/flot/jquery.flot.js"></script>
    <script src="vendors/flot/jquery.flot.pie.js"></script>
    <script src="vendors/flot/jquery.flot.categories.js"></script>
    <script src="vendors/flot/jquery.flot.time.js"></script>
    <script src="vendors/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="vendors/angular/angular.min.js"></script>
    <script src="vendors/angular/angular-route.min.js"></script>
    <script src="vendors/angular/angular-resource.min.js"></script>
    <script type="text/javascript">
        var dashboardApp = angular.module('dashboardApp', ['ngRoute', 'ngResource']);
        var base_url = 'http://localhost/world/api/public/';
        dashboardApp.config(['$routeProvider', function($routeProvider){
            $routeProvider.
                    when('/', {
                        templateUrl:'partials/dashboard-main',
                        controller:'dashboardController',
                    }).
                     when('/language', {
                        templateUrl:'partials/dashboard-language',
                        controller:'dashboardCountryLanguageController',
                    }).
                    when('/country', {
                        templateUrl:'partials/dashboard-country',
                        controller:'dashboardCountryController',
                    }).
                    when('/city', {
                        templateUrl:'partials/dashboard-city',
                        controller:'dashboardCityController',
                    }).
                    when('/continent', {
                        templateUrl:'partials/dashboard-continent',
                        controller:'dashboardContinentController',
                    }).
                    when('/region', {
                        templateUrl:'partials/dashboard-region',
                        controller:'dashboardRegionController',
                    }).
                    otherwise({
                        redirectTo: '/'
                    });
        }]);
        
        dashboardApp.filter('spaceless',function(){
            return function(input){
                return input.replace(/\s+/g,'_');
            }
        });

        dashboardApp.directive('simpleLoading', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.simpleLoading, function(value) {
                        console.log(value);
                        if (value) element.show();
                        else element.hide();
                    });
                }
            };
        });

        /* BEGIN: dashboard main */
        
        dashboardApp.directive('countryContinentPieChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    $http.get(base_url+'country/continent/area').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push({
                                                label:item.Name, 
                                                data:item.TotalCountries,
                                            });
                        }
                        setTimeout(function(){
                            element.html('');
                            $.plot(element, chart_data, {
                                series: {
                                    pie: {
                                        show: true
                                    }
                                },
                                grid: {
                                    hoverable: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%p.0%, %s",
                                    shifts: {
                                        x: 20,
                                        y: 0
                                    },
                                    defaultTheme: false
                                }
                            });

                            console.log('chart loaded');
                        }, 1000);
                    });

                    
                }
            };
        });
        
        dashboardApp.directive('countryRegionBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/region/area').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalCountries, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                    }
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1500);
                    });
                }
            };
        });
        
        dashboardApp.directive('worldwideLanguageBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country_lang/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                    }
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        dashboardApp.directive('topTenCountryCityBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/city/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#0000FF", "#00FFFF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('topTenCountryLanguageBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/lang/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF7070", "#0022FF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1800);
                    });
                }
            };
        });
        
        /* END: dashboard main */

        /* BEGIN: dashboard country */

        dashboardApp.directive('topTenCountryPopulationBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/population/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.Population, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#0000FF", "#00FFFF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('topTenCountryLifeExpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/lifeexp/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.LifeExpectancy, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#C8C627", "#C6C400"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('topTenCountrySurfaceAreaBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/surfacearea/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.SurfaceArea, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#C8C627", "#C6C400"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('countryHeadOfStateBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/head/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF7070", "#0022FF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('countryGnpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/gnp/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.GNP, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF7070", "#0022FF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('topTenCountryGovtBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/govt/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#3F981F", "#2A9802"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('topTenLongestSovereignCountryBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/indep_year/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.Sovereign, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#3F981F", "#2A9802"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });    
        
        /* END: dashboard country */

        /* BEGIN: dashboard language */

        dashboardApp.directive('topTenWorldLanguageBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country_lang/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#0000FF", "#00FFFF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        /* END: dashboard language */

        /* BEGIN: dashboard city */

        dashboardApp.directive('topTenCityPopulationBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'city/population/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.Population]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#0000FF", "#00FFFF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });

        dashboardApp.directive('cityContinentBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'city/continent').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                colors: ["#0000FF", "#00FFFF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('cityContinentPieChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'city/continent').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push({
                                                label:item.Name, 
                                                data:item.TotalItem,
                                            });
                        }
                        setTimeout(function(){
                            $.plot(element, chart_data, {
                                series: {
                                    pie: {
                                        show: true
                                    }
                                },
                                grid: {
                                    hoverable: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%p.0%, %s",
                                    shifts: {
                                        x: 20,
                                        y: 0
                                    },
                                    defaultTheme: false
                                }
                            });

                            console.log('chart loaded');
                        }, 1000);
                    });

                    
                }
            };
        });

        dashboardApp.directive('cityRegionBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'city/region').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                colors: ["#0000FF", "#00FFFF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        /* END: dashboard continent */

        /* BEGIIN: dashboard continent */

        dashboardApp.directive('continentSurfaceAreaPieChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/SurfaceArea/continent').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push({
                                                label:item.Name, 
                                                data:item.SurfaceArea,
                                            });
                        }
                        setTimeout(function(){
                            $.plot(element, chart_data, {
                                series: {
                                    pie: {
                                        show: true
                                    }
                                },
                                grid: {
                                    hoverable: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%p.0%, %s",
                                    shifts: {
                                        x: 20,
                                        y: 0
                                    },
                                    defaultTheme: false
                                }
                            });

                            console.log('chart loaded');
                        }, 1000);
                    });

                    
                }
            };
        });
        
        dashboardApp.directive('continentPopulationBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/Population/continent').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.Population]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                colors: ["#0000FF", "#00FFFF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('continentLifeExpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/LifeExpectancy/continent').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.LifeExpectancy, item.Name ]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                colors: ["#FF7070", "#0022FF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('continentGnpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/GNP/continent').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.GNP]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                colors: ["#3F981F", "#2A9802"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        /* END: dashboard continent */

        /* BEGIN: dashboard region */
        
        dashboardApp.directive('regionSurfaceAreaPieChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/SurfaceArea/region').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push({
                                                label:item.Name, 
                                                data:item.SurfaceArea,
                                            });
                        }
                        setTimeout(function(){
                            $.plot(element, chart_data, {
                                series: {
                                    pie: {
                                        show: true
                                    }
                                },
                                grid: {
                                    hoverable: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%p.0%, %s",
                                    shifts: {
                                        x: 20,
                                        y: 0
                                    },
                                    defaultTheme: false
                                }
                            });

                            console.log('chart loaded');
                        }, 1000);
                    });

                    
                }
            };
        });
        
        dashboardApp.directive('regionPopulationBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/Population/region').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Population, item.Name]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                colors: ["#0000FF", "#00FFFF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('regionLifeExpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/LifeExpectancy/region').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.LifeExpectancy, item.Name ]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                colors: ["#FF7070", "#0022FF"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });
        
        dashboardApp.directive('regionGnpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/GNP/region').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([ item.GNP, item.Name]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                colors: ["#3F981F", "#2A9802"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal: true,
                                        fill: true,
                                        fillColor: { colors: [{ opacity: 0.7 }, { opacity: 0.1}] }
                                    },
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%y (%x)"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 1900);
                    });
                }
            };
        });

        /* END: dashboard region */

        /* BEGIN: Controller */

        dashboardApp.controller('dashboardController', function($scope, $http){
            $scope.titles = "Dashboard";
        });

        dashboardApp.controller('dashboardCityController', function($scope, $http){
            $scope.titles = "City Infographic";
        });
        
        dashboardApp.controller('dashboardCountryController', function($scope, $http){
            $scope.titles = "Country Infographic";
        });
        
        dashboardApp.controller('dashboardCountryLanguageController', function($scope, $http){
            $scope.titles = "Country Language Infographic";
        });

        dashboardApp.controller('dashboardContinentController', function($scope, $http){
            $scope.titles = "Continent Infographic";
        });

        dashboardApp.controller('dashboardRegionController', function($scope, $http){
            $scope.titles = "Region Infographic";
        });

        /* END: Controller */
    </script>
</body>

</html>
