<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cretosphere - World Database</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendors/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendors/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper" ng-app="headStateApp">
        <div ng-include="'partials/template-nav'"></div>
        <div id="page-wrapper">
            <div ng-view></div>
        </div>
    </div>
    <!-- /#wrapper -->

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/metisMenu/dist/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="vendors/flot/excanvas.min.js"></script>
    <script src="vendors/flot/jquery.flot.js"></script>
    <script src="vendors/flot/jquery.flot.pie.js"></script>
    <script src="vendors/flot/jquery.flot.categories.js"></script>
    <!--
    <script src="vendors/flot/jquery.flot.resize.js"></script>
    -->
    <script src="vendors/flot/jquery.flot.time.js"></script>
    <script src="vendors/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="vendors/angular/angular.min.js"></script>
    <script src="vendors/angular/angular-route.min.js"></script>
    <script src="vendors/angular/angular-resource.min.js"></script>
    <script type="text/javascript">
        var headStateApp = angular.module('headStateApp', ['ngRoute', 'ngResource']);
        var base_url = 'http://localhost/world/api/public/';
        headStateApp.config(['$routeProvider', function($routeProvider){
            $routeProvider.
                    when('/', {
                        templateUrl:'partials/head-state-list',
                        controller:'headStateListController',
                    }).
                    otherwise({
                        redirectTo: '/'
                    });
        }]);

        headStateApp.directive('simpleLoading', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.simpleLoading, function(value) {
                        console.log(value);
                        if (value) element.show();
                        else element.hide();
                    });
                }
            };
        });

        headStateApp.filter('spaceless',function(){
            return function(input){
                return input.replace(/\s+/g,'_');
            }
        });

        headStateApp.controller('headStateListController', function($scope, $http){
            $scope.titles = "List of Head of State from different countries";
            $scope.showLoading = true;
            $http.get(base_url+'country/info/headofstate').success(function(data, status){
                $scope.headStates = data;
                $scope.showLoading = false;
            });
        });

    </script>
</body>

</html>
