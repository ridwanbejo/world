<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>World DB</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendors/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendors/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper" ng-app="govtFormApp">
        <div ng-include="'partials/template-nav'"></div>
        <div id="page-wrapper">
            <div ng-view></div>
        </div>
    </div>
    <!-- /#wrapper -->

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/metisMenu/dist/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="vendors/flot/excanvas.min.js"></script>
    <script src="vendors/flot/jquery.flot.js"></script>
    <script src="vendors/flot/jquery.flot.pie.js"></script>
    <script src="vendors/flot/jquery.flot.categories.js"></script>
    <!--
    <script src="vendors/flot/jquery.flot.resize.js"></script>
    -->
    <script src="vendors/flot/jquery.flot.time.js"></script>
    <script src="vendors/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="vendors/angular/angular.min.js"></script>
    <script src="vendors/angular/angular-route.min.js"></script>
    <script src="vendors/angular/angular-resource.min.js"></script>
    <script type="text/javascript">
        var govtFormApp = angular.module('govtFormApp', ['ngRoute', 'ngResource']);
        var base_url = 'http://localhost/world/api/public/';
        govtFormApp.config(['$routeProvider', function($routeProvider){
            $routeProvider.
                    when('/', {
                        templateUrl:'partials/govt-form-list',
                        controller:'govtFormListController',
                    }).
                    when('/:govt', {
                        templateUrl:'partials/govt-detail',
                        controller:'govtDetailController',
                    }).
                    when('/:govt/chart', {
                        templateUrl:'partials/govt-chart',
                        controller:'govtChartController',
                    }).
                    otherwise({
                        redirectTo: '/'
                    });
        }]);

        govtFormApp.directive('simpleLoading', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.simpleLoading, function(value) {
                        console.log(value);
                        if (value) element.show();
                        else element.hide();
                    });
                }
            };
        });

        govtFormApp.filter('spaceless',function(){
            return function(input){
                return input.replace(/\s+/g,'_');
            }
        });

        govtFormApp.directive('governmentGnpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/GNP'+'/governmentform/'+scope.government+'/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                    }
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        govtFormApp.directive('governmentSurfaceAreaBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/SurfaceArea'+'/governmentform/'+scope.government+'/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#3F981F", "#2A9802"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                    }
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });

        govtFormApp.directive('governmentLifeExpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/LifeExpectancy'+'/governmentform/'+scope.government+'/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF981F", "#FA9802"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                    }
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        govtFormApp.directive('governmentPopulationBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/Population'+'/governmentform/'+scope.government+'/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF98FF", "#FA980F"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                    }
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        govtFormApp.directive('governmentLanguageBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country_lang/'+scope.government+'/governmentform/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF98FF", "#FA980F"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                    }
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        govtFormApp.directive('governmentIndepYearBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/indep_year/'+scope.government+'/governmentform/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF98FF", "#FA980F"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                    }
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        govtFormApp.controller('govtFormListController', function($scope, $http){
            $scope.titles = "List of Governments Form";
            $scope.showLoading = true;
            $http.get(base_url+'country/info/governmentform').success(function(data, status){
                $scope.govtForms = data;
                $scope.showLoading = false;
            });
        });

        govtFormApp.controller('govtDetailController', function($scope, $http, $routeParams){
            $scope.titles = "Fact About - "+$routeParams.govt;
            $scope.showLoading = true;
            $http.get(base_url+'country/'+$routeParams.govt+"/govt").success(function(data, status){
                $scope.countries = data;
                $scope.showLoading = false;
            });
        });

        govtFormApp.controller('govtChartController', function($scope, $http, $routeParams){
            $scope.titles = "Chart - "+$routeParams.govt;
            $scope.government = $routeParams.govt;
        });

    </script>
</body>

</html>
