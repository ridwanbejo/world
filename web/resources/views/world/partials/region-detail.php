                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                This is a list of country in this region
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                               <div class="form-group input-group">
                                    <input class="form-control" type="text" ng-model="searchedCountry">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <div class="table-responsive table-bordered">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Country</th>
                                                <th>Region</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="country in countries | filter:searchedCountry">
                                                <td>{{ $index + 1 }}</td>
                                                <td>
                                                    {{ country.Name }} 
                                                </td>
                                                <td>
                                                    {{ country.Region }} 
                                                </td>
                                                <td>
                                                    <a href="country#/{{ country.Code | spaceless }}" class="btn btn-info btn-xs"><span class="fa fa-eye"></span></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
                <div class="alert alert-danger" simple-loading="showLoading" style="width:50%; left:35%;top:100px;position:fixed;margin:0 auto;text-align:center;">Loading ...</div>
                