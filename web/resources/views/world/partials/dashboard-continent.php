                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Continent By LifeExpectancy
                                </div>
                                <div class="panel-body">
                                    <div continent-surface-area-pie-chart style="width:100%;height:300px;">Loading...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Continent By Surface Area
                                </div>
                                <div class="panel-body">
                                    <div continent-life-exp-bar-chart style="width:100%;height:300px;">Loading...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Continent By Population
                            </div>
                            <div class="panel-body">
                                <div continent-population-bar-chart style="width:100%;height:300px;">Loading...</div>    
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Continent By Gross National Products
                            </div>
                            <div class="panel-body">
                                <div continent-gnp-bar-chart style="width:100%;height:300px;">Loading...</div>    
                            </div>
                        </div>
                    </div>
                </div>