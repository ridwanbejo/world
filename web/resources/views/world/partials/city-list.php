                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="form-group input-group">
                                        <input class="form-control" type="text" placeholder="search city here..." ng-model="q">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"  ng-click="searchCity();"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="#/new" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span> Add Item</a> 
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Displaying list of all country
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div dynamic="pager" class="pull-right">
                                    
                                </div>
                                <div style="margin-bottom:20px;margin-top:80px;"></div>
                                <div class="table-responsive table-bordered">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>CountryCode</th>
                                                <th>Name</th>
                                                <th>District</th>
                                                <th>Population</th>
                                                <th style="width:100px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="city in cities">
                                                <td>{{  (( currentPage - 1 ) * limitPage) + $index + 1 }}</td>
                                                <td><span class="label label-primary">{{ city.CountryCode }}</span></td>
                                                <td>{{ city.Name }}</td>
                                                <td>{{ city.District }}</td>
                                                <td>{{ city.Population }}</td>
                                                <td>
                                                    <a href="#/{{ city.ID }}" class="btn btn-success btn-sm"><span class="fa fa-pencil"></span></a> 
                                                    <a href="javascript:void(0);" ng-click="deleteCity(city.ID);" class="btn btn-danger btn-sm"><span class="fa fa-trash-o"></span></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div style="margin-bottom:20px;"></div>
                                <div dynamic="pager" class="pull-right">
                                    
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>