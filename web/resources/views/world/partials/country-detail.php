                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ country.Name }} ({{ country.Code }})</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a ng-click="deleteCountry(country.Code);" class="btn btn-sm btn-danger pull-right" style="margin-left:10px;"><span class="fa fa-trash-o"></span> Delete Forever</a>
                                <a href="#/edit/{{ country.Code }}" class="btn btn-sm btn-success pull-right"><span class="fa fa-pencil"></span> Edit This Country</a> 
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Factual Information
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive table-bordered">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td><b>Location</b></td>
                                                <td>:</td>
                                                <td>{{ country.Region}}, {{ country.Continent }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Population</b></td>
                                                <td>:</td>
                                                <td>{{ country.Population}}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Life Expectancy</b></td>
                                                <td>:</td>
                                                <td>{{ country.LifeExpectancy}}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Gross National Products</b></td>
                                                <td>:</td>
                                                <td>{{ country.GNP }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Surface Area</b></td>
                                                <td>:</td>
                                                <td>{{ country.SurfaceArea}}</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Governmental Information
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive table-bordered">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td><b>Head of State</b></td>
                                                <td>:</td>
                                                <td>{{ country.HeadOfState }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Government Form</b></td>
                                                <td>:</td>
                                                <td>{{ country.GovernmentForm }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Local Name</b></td>
                                                <td>:</td>
                                                <td>{{ country.LocalName }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Independent Year</b></td>
                                                <td>:</td>
                                                <td>{{ country.IndepYear }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Other Code</b></td>
                                                <td>:</td>
                                                <td>{{ country.Code2 }}</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Capitol Information
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive table-bordered">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td><b>Name</b></td>
                                                <td>:</td>
                                                <td>{{ capital.Name }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>District</b></td>
                                                <td>:</td>
                                                <td>{{ capital.District }}</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>Population</b></td>
                                                <td>:</td>
                                                <td>{{ capital.Population }}</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Spoken Language
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <a style="margin-bottom:25px;" data-toggle="modal" ng-click="addLanguage();" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span> Add Language</a> 
                                <div class="table-responsive table-bordered">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Language</th>
                                                <th>Percentage</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="lang in country_lang">
                                                <td>{{ $index + 1 }}</td>
                                                <td>
                                                    {{ lang.Language }} 
                                                    <span style="{{ (lang.IsOfficial == 'T' ? '' : 'display:none;') }}"class="label label-primary">official</span>
                                                </td>
                                                <td>{{ lang.Percentage }}</td>
                                                <td>
                                                    <a href="javascript:void(0);" ng-click="editLanguage(country.Code, lang.Language);" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                    <a href="javascript:void(0);" ng-click="deleteLanguage(country.Code, lang.Language, $index);" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                City List
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <a style="margin-bottom:25px;" href="javascript:void(0);" ng-click="addCity();" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span> Add City</a> 
                                <div class="table-responsive table-bordered">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>District</th>
                                                <th>Population</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="city in cities">
                                                <td>{{ $index + 1 }}</td>
                                                <td>
                                                    {{ city.Name }} 
                                                </td>
                                                <td>{{ city.District }}</span></td>
                                                <td>{{ city.Population }}</span></td>
                                                <td>
                                                    <a href="javascript:void(0);" ng-click="setCapital(city.ID);" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-globe"></span></a>
                                                    <a href="javascript:void(0);" ng-click="editCity(city.ID);" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                    <a href="javascript:void(0);" ng-click="deleteCity(city.ID, $index);"  class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
                <div class="modal fade" ak-modal="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="showModal=false">&times;</button>
                        <h4 class="modal-title">{{ modalTitle }}</h4>
                      </div>
                      <div class="modal-body">
                        <div dynamic="modalContent"></div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="showModal=false">Close</button>
                        <button type="button" class="btn btn-primary" data-modal-type="{{ modalType }}" ng-click="submitForm(modalType, modalAction);">Save changes</button>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <div class="alert alert-danger" simple-loading="showLoading" style="width:50%; left:35%;top:100px;position:fixed;margin:0 auto;text-align:center;">Loading ...</div>
                