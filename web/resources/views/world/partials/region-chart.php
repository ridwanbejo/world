                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 biggest GNP country accross the region
                                </div>
                                <div class="panel-body">
                                    <div region-gnp-bar-chart  ng-model="region" style="width:100%;height:300px;">Loading ...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 widest country accross the region
                                </div>
                                <div class="panel-body">
                                    <div region-surface-area-bar-chart ng-model="region" style="width:100%;height:300px;">Loading ...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 language accross the region
                                </div>
                                <div class="panel-body">
                                    <div region-language-bar-chart  ng-model="region" style="width:100%;height:300px;">Loading ...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 highest life expectancy country accross the region
                                </div>
                                <div class="panel-body">
                                    <div region-life-exp-bar-chart ng-model="region" style="width:100%;height:300px;">Loading ...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 densely populated country accross the region
                            </div>
                            <div class="panel-body">
                                <div region-population-bar-chart  ng-model="region" style="width:100%;height:300px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 longest established country accross the region
                            </div>
                            <div class="panel-body">
                                <div region-indep-year-bar-chart  ng-model="region" style="width:100%;height:300px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 government form adopt by country accross the region
                            </div>
                            <div class="panel-body">
                                <div region-govt-form-bar-chart  ng-model="region" style="width:100%;height:300px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger" simple-loading="showLoading" style="width:50%; left:35%;top:100px;position:fixed;margin:0 auto;text-align:center;">Loading ...</div>
                