                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="form-group input-group">
                                        <input class="form-control" type="text" ng-model="searchedItem">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Displaying list of head of state from each their countries
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive table-bordered">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Head of State</th>
                                                <th style="width:100px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in headStates | filter:searchedItem">
                                                <td>{{ $index+1 }}</td>
                                                <td>{{ item.Name }}</td>
                                                <td>
                                                    <a href="#/" class="btn btn-info btn-sm"><span class="fa fa-eye"></span></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>

                        <!-- /.panel -->
                    </div>
                </div>
                <div class="alert alert-danger" simple-loading="showLoading" style="width:50%; left:35%;top:100px;position:fixed;margin:0 auto;text-align:center;">Loading ...</div>
                