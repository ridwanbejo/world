                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 biggest GNP country accross the continent
                                </div>
                                <div class="panel-body">
                                    <div continent-gnp-bar-chart  ng-model="continent" style="width:100%;height:300px;">Loading ...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 widest country accross the continent
                                </div>
                                <div class="panel-body">
                                    <div continent-surface-area-bar-chart ng-model="continent" style="width:100%;height:300px;">Loading ...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 language accross the continent
                                </div>
                                <div class="panel-body">
                                    <div continent-language-bar-chart  ng-model="continent" style="width:100%;height:300px;">Loading ...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 highest life expectancy country accross the continent
                                </div>
                                <div class="panel-body">
                                    <div continent-life-exp-bar-chart ng-model="continent" style="width:100%;height:300px;">Loading ...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 densely populated country accross the continent
                            </div>
                            <div class="panel-body">
                                <div continent-population-bar-chart  ng-model="continent" style="width:100%;height:300px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 longest established country accross the continent
                            </div>
                            <div class="panel-body">
                                <div continent-indep-year-bar-chart  ng-model="continent" style="width:100%;height:300px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 government form adopt by country accross the continent
                            </div>
                            <div class="panel-body">
                                <div continent-govt-form-bar-chart  ng-model="continent" style="width:100%;height:300px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger" simple-loading="showLoading" style="width:50%; left:35%;top:100px;position:fixed;margin:0 auto;text-align:center;">Loading ...</div>
                