                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Country by Continent
                                </div>
                                <div class="panel-body">
                                    <div country-continent-pie-chart style="width:100%;height:300px;">Loading...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Country by Region
                                </div>
                                <div class="panel-body">
                                    <div country-region-bar-chart style="width:100%;height:300px;">Loading...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 Worldwide Language
                            </div>
                            <div class="panel-body">
                                <div worldwide-language-bar-chart style="width:100%;height:300px;">Loading...</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 Country By Language
                                </div>
                                <div class="panel-body">
                                    <div top-ten-country-language-bar-chart style="width:100%;height:300px;">Loading...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 Country By City
                                </div>
                                <div class="panel-body">
                                    <div top-ten-country-city-bar-chart style="width:100%;height:300px;">Loading...</div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>