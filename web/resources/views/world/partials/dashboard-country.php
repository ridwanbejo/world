                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 Country By Population
                            </div>
                            <div class="panel-body">
                                <div top-ten-country-population-bar-chart style="width:100%;height:300px;">Loading...</div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 Country By SurfaceArea
                                </div>
                                <div class="panel-body">
                                    <div top-ten-country-surface-area-bar-chart style="width:100%;height:300px;">Loading...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 Country By Life Expectancy
                                </div>
                                <div class="panel-body">
                                    <div top-ten-country-life-exp-bar-chart style="width:100%;height:300px;">Loading...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 Country by GNP
                                </div>
                                <div class="panel-body">
                                    <div country-gnp-bar-chart style="width:100%;height:300px;">Loading...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top Head of State By Country They Rules
                                </div>
                                <div class="panel-body">
                                    <div country-head-of-state-bar-chart style="width:100%;height:300px;">Loading...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 Governtment Form established among countries
                                </div>
                                <div class="panel-body">
                                    <div top-ten-country-govt-bar-chart style="width:100%;height:300px;">Loading...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 Longest Sovereign Country
                                </div>
                                <div class="panel-body">
                                    <div top-ten-longest-sovereign-country-bar-chart style="width:100%;height:300px;">Loading...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>