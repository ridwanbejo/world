                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Modify the city by change the below form
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <form role="form">
                                    <div class="form-group">
                                        <label>Country Code</label>
                                        <input class="form-control" autocomplete ng-model="city.CountryCode" value="{{ city.CountryCode }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" ng-model="city.Name" value="{{ city.Name }}">
                                    </div>
                                    <div class="form-group">
                                        <label>District</label>
                                        <input class="form-control" ng-model="city.District" value="{{ city.District }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Population</label>
                                        <input class="form-control" ng-model="city.Population" value="{{ city.Population }}">
                                    </div>
                                    <a href="javascript:void(0);" ng-click="submitTrip();" class="btn btn-primary"><span class="fa fa-send"></span> Submit</a>
                                    <a href="#/" class="btn btn-danger"><span class="fa fa-times"></span> Cancel</a>
                                </form>
                               
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>