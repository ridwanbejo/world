                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="form-group input-group">
                                        <input class="form-control" type="text" ng-model="q" >
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" ng-click="searchCountry();"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <!-- <a href="#/edit/{{ country.Code }}" class="btn btn-sm btn-primary pull-right"><span class="fa fa-plus"></span> Add Item</a>  -->
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Displaying list of all country language
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div dynamic="pager" class="pull-right">
                                    
                                </div>
                                <div style="margin-bottom:20px;margin-top:80px;"></div>
                                <div class="table-responsive table-bordered">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Language</th>
                                                <th>Total Used</th>
                                                <th style="width:100px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="lang in country_lang">
                                                <td>{{  (( currentPage - 1 ) * limitPage) + $index + 1 }}</td>
                                                <td>{{ lang.Language }}</td>
                                                <td>{{ lang.TotalUsed }}</td>
                                                <td>
                                                    <a href="#/{{ lang.Language | spaceless }}" class="btn btn-info btn-sm"><span class="fa fa-eye"></span></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div style="margin-bottom:20px;"></div>
                                <div dynamic="pager" class="pull-right">
                                    
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>

                        <!-- /.panel -->
                    </div>
                </div>
                <div class="alert alert-danger" simple-loading="showLoading" style="width:50%; left:35%;top:100px;position:fixed;margin:0 auto;text-align:center;">Loading ...</div>
                