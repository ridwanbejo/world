                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 City by Population
                            </div>
                            <div class="panel-body">
                                <div top-ten-city-population-bar-chart style="width:100%;height:300px;">Loading ...</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Total City in Continent
                            </div>
                            <div class="panel-body">
                                <div city-continent-pie-chart style="width:100%;height:300px;">Loading ...</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Total City in Region
                            </div>
                            <div class="panel-body">
                                <div city-region-bar-chart style="width:100%;height:300px;">Loading ...</div>
                            </div>
                        </div>
                    </div>
                </div>