                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 Worldwide Language
                            </div>
                            <div class="panel-body">
                                <div top-ten-world-language-bar-chart style="width:100%;height:300px;">Loading...</div>
                            </div>
                        </div>
                    </div>
                    
                </div>