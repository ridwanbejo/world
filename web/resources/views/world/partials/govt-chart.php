                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 biggest GNP country among the government
                                </div>
                                <div class="panel-body">
                                    <div government-gnp-bar-chart  ng-model="government" style="width:100%;height:300px;">Loading ...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 widest country among the government
                                </div>
                                <div class="panel-body">
                                    <div government-surface-area-bar-chart ng-model="government" style="width:100%;height:300px;">Loading ...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 language among the government
                                </div>
                                <div class="panel-body">
                                    <div government-language-bar-chart  ng-model="government" style="width:100%;height:300px;">Loading ...</div>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Top 10 highest life expectancy country among the government
                                </div>
                                <div class="panel-body">
                                    <div government-life-exp-bar-chart ng-model="government" style="width:100%;height:300px;">Loading ...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 densely populated country among the government
                            </div>
                            <div class="panel-body">
                                <div government-population-bar-chart  ng-model="government" style="width:100%;height:300px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Top 10 longest established country among the government
                            </div>
                            <div class="panel-body">
                                <div government-indep-year-bar-chart  ng-model="government" style="width:100%;height:300px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger" simple-loading="showLoading" style="width:50%; left:35%;top:100px;position:fixed;margin:0 auto;text-align:center;">Loading ...</div>
                