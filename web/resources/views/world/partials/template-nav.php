<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">CratoSphere</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?php echo url(); ?>/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="<?php echo url(); ?>/dashboard#/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="javascript:void(0);"><i class="fa fa-bar-chart-o fa-fw"></i> Infographics<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo url(); ?>/dashboard#/country">Country</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/dashboard#/city">City</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/dashboard#/language">Language</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/dashboard#/continent">Continent</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/dashboard#/region">Region</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);"><i class="fa fa-table fa-fw"></i> Master<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo url(); ?>/country#/">Country</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/city#/">City</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);"><i class="fa fa-table fa-fw"></i> Extra<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?php echo url(); ?>/country_lang#/">Language</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/government_form#/">Government Form</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/head_of_state#/">Head Of State</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/continent#/">Continents</a>
                        </li>
                        <li>
                            <a href="<?php echo url(); ?>/region#/">Region</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>