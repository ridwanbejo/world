                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Region By Surface Area
                            </div>
                            <div class="panel-body">
                                <div region-surface-area-pie-chart style="width:100%;height:400px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Region By Population
                            </div>
                            <div class="panel-body">
                                <div region-population-bar-chart style="width:100%;height:400px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Region By LifeExpectancy
                            </div>
                            <div class="panel-body">
                                <div region-life-exp-bar-chart style="width:100%;height:400px;">Loading ...</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Region By Gross National Products
                            </div>
                            <div class="panel-body">
                                <div region-gnp-bar-chart style="width:100%;height:400px;">Loading ...</div>    
                            </div>
                        </div>
                    </div>
                </div>