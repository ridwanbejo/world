                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="form-group input-group">
                                        <input class="form-control" type="text" placeholder="search country here..." ng-model="q" ng-keyup="searchCountry();">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="#/new" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span> Add Item</a> 
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Displaying list of all country
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div dynamic="pager" class="pull-right">
                                    
                                </div>
                                <div style="margin-bottom:20px;margin-top:80px;"></div>
                                <div class="table-responsive table-bordered">

                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th>More Information</th>
                                                <th style="width:100px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="country in countries">
                                                <td>{{  (( currentPage - 1 ) * limitPage) + $index + 1 }}</td>
                                                <td><span class="label label-primary">{{ country.Code }}</span></td>
                                                <td>{{ country.Name }}</td>
                                                <td>{{ country.Region }}, {{ country.Continent }}</td>
                                                <td>
                                                    <a href="#/{{ country.Code }}" class="btn btn-success btn-sm"><span class="fa fa-eye"></span></a> 
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div style="margin-bottom:20px;"></div>
                                <div dynamic="pager" class="pull-right">
                                    
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                </div>
                <div class="alert alert-danger" simple-loading="showLoading" style="width:50%; left:35%;top:100px;position:fixed;margin:0 auto;text-align:center;">Loading ...</div>
                