                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">{{ titles }}</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form role="form">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Factual Information
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input class="form-control" value="{{ country.Name }}" ng-model="country.Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Region</label>
                                        <input class="form-control" region-autocomplete value="{{ country.Region }}" ng-model="country.Region">
                                    </div>
                                    <div class="form-group">
                                        <label>Continent</label>
                                        <input class="form-control" continent-autocomplete value="{{ country.Continent }}" ng-model="country.Continent">
                                    </div>
                                    <div class="form-group">
                                        <label>Population</label>
                                        <input class="form-control" value="{{ country.Population }}" ng-model="country.Population">
                                    </div>
                                    <div class="form-group">
                                        <label>Gross National Products</label>
                                        <input class="form-control" value="{{ country.GNP }}" ng-model="country.GNP">
                                    </div>
                                    <div class="form-group">
                                        <label>Surface Area</label>
                                        <input class="form-control" value="{{ country.SurfaceArea }}" ng-model="country.SurfaceArea">
                                    </div>
                                    <div class="form-group">
                                        <label>Life Expectancy</label>
                                        <input class="form-control" value="{{ country.LifeExpectancy }}" ng-model="country.LifeExpectancy">
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Governmental Information
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Head of State</label>
                                        <input class="form-control" value="{{ country.HeadOfState }}" ng-model="country.HeadOfState">
                                    </div>
                                    <div class="form-group">
                                        <label>Local Name</label>
                                        <input class="form-control" value="{{ country.LocalName }}" ng-model="country.LocalName">
                                    </div>
                                    <div class="form-group">
                                        <label>Independent Year</label>
                                        <input class="form-control" value="{{ country.IndepYear }}" ng-model="country.IndepYear">
                                    </div>
                                    <div class="form-group">
                                        <label>Government Form</label>
                                        <input class="form-control" govt-autocomplete value="{{ country.GovernmentForm }}" ng-model="country.GovernmentForm">
                                    </div>
                                    <div class="form-group">
                                        <label>Code</label>
                                        <input class="form-control" value="{{ country.Code }}" ng-model="country.Code">
                                    </div>
                                    <div class="form-group">
                                        <label>Other Code</label>
                                        <input class="form-control" value="{{ country.Code2 }}" ng-model="country.Code2">
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <a ng-click="submitForm();" class="btn btn-sm btn-success"><span class="fa fa-pencil"></span> Submit</a> 
                                    <a href="#/{{ country.Code }}" class="btn btn-sm btn-danger" style="margin-left:10px;"><span class="fa fa-trash-o"></span> Cancel</a>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        </form>
                        <!-- /.panel -->
                    </div>
                </div>