<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cretosphere - World Database</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendors/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendors/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper" ng-app="regionApp">
        <div ng-include="'partials/template-nav'"></div>
        <div id="page-wrapper">
            <div ng-view></div>
        </div>
    </div>
    <!-- /#wrapper -->

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/metisMenu/dist/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="vendors/flot/excanvas.min.js"></script>
    <script src="vendors/flot/jquery.flot.js"></script>
    <script src="vendors/flot/jquery.flot.pie.js"></script>
    <script src="vendors/flot/jquery.flot.categories.js"></script>
    <!--
    <script src="vendors/flot/jquery.flot.resize.js"></script>
    -->
    <script src="vendors/flot/jquery.flot.time.js"></script>
    <script src="vendors/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="vendors/angular/angular.min.js"></script>
    <script src="vendors/angular/angular-route.min.js"></script>
    <script src="vendors/angular/angular-resource.min.js"></script>
    <script type="text/javascript">
        var regionApp = angular.module('regionApp', ['ngRoute', 'ngResource']);
        var base_url = 'http://localhost/world/api/public/';
        regionApp.config(['$routeProvider', function($routeProvider){
            $routeProvider.
                    when('/', {
                        templateUrl:'partials/region-list',
                        controller:'regionListController',
                    }).
                    when('/:region', {
                        templateUrl:'partials/region-detail',
                        controller:'regionDetailController',
                    }).
                    when('/:region/chart', {
                        templateUrl:'partials/region-chart',
                        controller:'regionChartController',
                    }).
                    otherwise({
                        redirectTo: '/'
                    });
        }]);

        regionApp.directive('simpleLoading', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.simpleLoading, function(value) {
                        console.log(value);
                        if (value) element.show();
                        else element.hide();
                    });
                }
            };
        });

        regionApp.filter('spaceless',function(){
            return function(input){
                return input.replace(/\s+/g,'_');
            }
        });

        regionApp.directive('regionGnpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/GNP'+'/region/'+scope.region+'/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                    }
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        regionApp.directive('regionSurfaceAreaBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/SurfaceArea'+'/region/'+scope.region+'/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#3F981F", "#2A9802"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                    }
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });

        regionApp.directive('regionLifeExpBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/LifeExpectancy'+'/region/'+scope.region+'/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalItem, item.Name]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF981F", "#FA9802"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                    }
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        regionApp.directive('regionPopulationBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/Population'+'/region/'+scope.region+'/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF98FF", "#FA980F"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                    }
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        regionApp.directive('regionLanguageBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country_lang/'+scope.region+'/region/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF98FF", "#FA980F"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                    }
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        regionApp.directive('regionIndepYearBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/indep_year/'+scope.region+'/region/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF98FF", "#FA980F"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                    }
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });
        
        regionApp.directive('regionGovtFormBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/govt/'+scope.region+'/region/top').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.Name, item.TotalItem]);
                        }

                        setTimeout(function(){
                            element.html('');
                            var barOptions = {
                                colors: ["#FF98FF", "#FA980F"],
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                    }
                                },
                                xaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });

        regionApp.controller('regionListController', function($scope, $http){
            $scope.titles = "List of Region";
            $scope.showLoading = true;
            $http.get(base_url+'country/info/region').success(function(data, status){
                $scope.region = data;
                $scope.showLoading = false;
            });
        });

        regionApp.controller('regionDetailController', function($scope, $http, $routeParams){
            $scope.titles = "Fact About - "+$routeParams.region;
            $scope.showLoading = true;
            $http.get(base_url+'country/region/'+$routeParams.region).success(function(data, status){
                $scope.countries = data;
                $scope.showLoading = false;
            });
        });

        regionApp.controller('regionChartController', function($scope, $http, $routeParams){
            $scope.titles = "Chart - "+$routeParams.region;
            $scope.region = $routeParams.region;
        });
    </script>
</body>

</html>
