<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cretosphere - World Database</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendors/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendors/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper" ng-app="countryLangApp">
        <div ng-include="'partials/template-nav'"></div>
        <div id="page-wrapper">
            <div ng-view></div>
        </div>
    </div>
    <!-- /#wrapper -->

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/metisMenu/dist/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="vendors/flot/excanvas.min.js"></script>
    <script src="vendors/flot/jquery.flot.js"></script>
    <script src="vendors/flot/jquery.flot.pie.js"></script>
    <script src="vendors/flot/jquery.flot.categories.js"></script>
    <!--
    <script src="vendors/flot/jquery.flot.resize.js"></script>
    -->
    <script src="vendors/flot/jquery.flot.time.js"></script>
    <script src="vendors/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="vendors/angular/angular.min.js"></script>
    <script src="vendors/angular/angular-route.min.js"></script>
    <script src="vendors/angular/angular-resource.min.js"></script>
    <script type="text/javascript">
        var countryLangApp = angular.module('countryLangApp', ['ngRoute', 'ngResource']);
        var base_url = 'http://localhost/world/api/public/';
        
        countryLangApp.config(['$routeProvider', function($routeProvider){
            $routeProvider.
                    when('/', {
                        templateUrl:'partials/country-lang-list',
                        controller:'CountryLangListController',
                    }).
                    when('/:countryName', {
                        templateUrl:'partials/country-lang-detail',
                        controller:'CityDetailController',
                    }).
                    otherwise({
                        redirectTo: '/'
                    });
        }]);

        countryLangApp.directive('simpleLoading', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.simpleLoading, function(value) {
                        if (value) element.show();
                        else element.hide();
                    });
                }
            };
        });

        countryLangApp.directive('dynamic', function ($compile) {
          return {
            restrict: 'A',
            replace: true,
            link: function (scope, ele, attrs) {
              scope.$watch(attrs.dynamic, function(html) {
                ele.html(html);
                $compile(ele.contents())(scope);
              });
            }
          };
        });

        countryLangApp.directive('continentPieChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/'+scope.countryName+'/lang/continent').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push({
                                                label:item.Name, 
                                                data:item.TotalCountries,
                                            });
                        }
                        setTimeout(function(){
                            $.plot(element, chart_data, {
                                series: {
                                    pie: {
                                        show: true
                                    }
                                },
                                grid: {
                                    hoverable: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "%p.0%, %s",
                                    shifts: {
                                        x: 20,
                                        y: 0
                                    },
                                    defaultTheme: false
                                }
                            });

                            console.log('chart loaded');
                        }, 1000);
                    });

                    
                }
            };
        });
        
        countryLangApp.directive('regionBarChart', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    
                    $http.get(base_url+'country/'+scope.countryName+'/lang/region').success(function(result, status){
                        var chart_data = [];

                        for (var i = 0; i < result.length; i++ )
                        {
                            item = result[i];
                            chart_data.push([item.TotalCountries, item.Name]);
                        }

                        setTimeout(function(){
                            var barOptions = {
                                series: {
                                    bars: {
                                        show: true,
                                        barWidth: 0.6,
                                        align: "center",
                                        horizontal:true,
                                    }
                                },
                                yaxis: {
                                    mode: "categories",
                                    tickLength: 0
                                },
                                grid: {
                                    hoverable: true
                                },
                                legend: {
                                    show: true
                                },
                                tooltip: true,
                                tooltipOpts: {
                                    content: "x: %x, y: %y"
                                }
                            };
                            
                            $.plot(element, [ chart_data ], barOptions);

                            console.log('chart loaded');
                        }, 2000);
                    });
                }
            };
        });

        countryLangApp.filter('spaceless',function(){
            return function(input){
                return input.replace(/\s+/g,'_');
            }
        });

        countryLangApp.controller('CountryLangListController', function($scope, $http){
            $scope.titles = "List of Country Language";
            $scope.showLoading = true;
            $scope.currentPage = 1;
            $scope.totalPage = 1;
            $scope.limitPage = 10;

            $http.get(base_url+'country_lang/all?page='+$scope.currentPage+'&limit='+$scope.limitPage).success(function(data, status){
                $scope.showLoading = false;
                $scope.country_lang = data.result;
                $scope.totalPage = data.total_page;
                $scope.pager = createPager(1, $scope.totalPage);
            });

            $scope.searchCountry = function(){
                $http.get(base_url+'country_lang/all?q='+$scope.q).success(function(data, status){
                    $scope.country_lang = data.result;
                });
                $scope.currentPage = 1;
            }

            $scope.changePage = function(page){
                $scope.showLoading = true;
                $http.get(base_url+'country_lang/all?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.country_lang = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });
                
                console.log(page);
                
                $scope.currentPage = page;
            }

            $scope.prevPage = function(){
                
                if ($scope.currentPage > 1)
                {
                    var page = $scope.currentPage - 1;
                }
                else
                {
                    var page = 1;
                }

                console.log(page);
                
                $scope.showLoading = true;
                $http.get(base_url+'country_lang/all?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.country_lang = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });
                $scope.currentPage = page;
            }

            $scope.nextPage = function(){
                if ($scope.currentPage < $scope.totalPage)
                {
                    var page = $scope.currentPage + 1;
                }
                else
                {
                    var page = $scope.totalPage;
                }

                console.log(page);
                
                $scope.showLoading = true;
                $http.get(base_url+'country_lang/all?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.country_lang = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });

                $scope.currentPage = page;
            }

            function createPager(page, total)
            {
                console.log(page);
                        
                var pager_length = 5;
                var pager = "";
                if (page >= pager_length)
                {
                    if (page > total - pager_length && page <= total)
                    {
                        console.log(' page akhir ');
                        pager = '<ul class="pagination">'+
                                   '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                        
                        for (var i = total - pager_length  + 1; i <= total; i ++)
                        {
                          pager += '<li><a href="javascript:void(0);" ng-click="changePage('+ ( i ) +');">'+ ( i ) +'</a></li>';
                        }          
                            
                        if (page < total)
                        {
                            pager +=   '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>';
                        }
                        
                        pager +=   '</ul>';
                    }
                    else
                    {
                        console.log('page tengah');

                        pager = '<ul class="pagination">'+
                                   '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                        
                        for (var i = 0; i < 5; i ++)
                        {
                          pager += '<li><a href="javascript:void(0);" ng-click="changePage('+ (page + i ) +');">'+ (page + i ) +'</a></li>';
                        }          
                            
                        pager +=   '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>'+
                                '</ul>';
                    }
                    
                }
                else if (page < pager_length)
                {
                    console.log('page awal');
                    var prev ="";
                    if (page > 1)
                    {
                        prev = '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                    
                    }
                   
                    pager = '<ul class="pagination">'+
                              prev+
                              '<li><a href="javascript:void(0);" ng-click="changePage(1);">1</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(2);">2</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(3);">3</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(4);">4</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(5);">5</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>'+
                            '</ul>';
                }
                

                return pager;
            }
        });

        countryLangApp.controller('CityDetailController', function($scope, $http, $routeParams){
            $scope.showLoading = true;
            $scope.titles = "Information About - "+$routeParams.countryName;
            $scope.countryName = $routeParams.countryName;

            $http.get(base_url+'country/'+$routeParams.countryName+'/lang/region').success(function(data, status){
                $scope.regions = data;
            });

            $http.get(base_url+'country/'+$routeParams.countryName+'/lang').success(function(data, status){
                $scope.showLoading = false;
                $scope.countries = data;
            });
        });
    </script>
</body>

</html>
