<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cretosphere - World Database</title>
    
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="vendors/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendors/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" type="text/css">
</head>

<body>

    <div id="wrapper" ng-app="cityApp">
        <div ng-include="'partials/template-nav'"></div>
        <div id="page-wrapper">
            <div ng-view></div>
        </div>
    </div>
    <!-- /#wrapper -->

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/metisMenu/dist/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
    <script src="vendors/angular/angular.min.js"></script>
    <script src="vendors/angular/angular-route.min.js"></script>
    <script src="vendors/angular/angular-resource.min.js"></script>
    <script type="text/javascript">
        var cityApp = angular.module('cityApp', ['ngRoute', 'ngResource']);
        var base_url = 'http://localhost/world/api/public/';
        
        cityApp.config(['$routeProvider', function($routeProvider){
            $routeProvider.
                        when('/', {
                            templateUrl:'partials/city-list',
                            controller:'CityListController',
                        }).
                        when('/new', {
                            templateUrl:'partials/city-edit',
                            controller:'CityNewController',
                        }).
                        when('/:cityId', {
                            templateUrl:'partials/city-edit',
                            controller:'CityEditController',
                        }).
                        otherwise({
                            redirectTo: '/'
                        });
        }]);
        
        cityApp.directive('autocomplete', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.autocomplete({
                        source:function (request, response){
                            $http.get(base_url+'country?q='+scope.city.CountryCode).success(function(data){
                                result = [];
                                for (var i = 0; i < data.length; i++){
                                    result.push({'label':data[i].Name, 'value':data[i].Code});
                                }
                                response(result);
                            });
                        },
                    });
                }
            };
        });

        cityApp.directive('dynamic', function ($compile) {
          return {
            restrict: 'A',
            replace: true,
            link: function (scope, ele, attrs) {
              scope.$watch(attrs.dynamic, function(html) {
                ele.html(html);
                $compile(ele.contents())(scope);
              });
            }
          };
        });

        cityApp.directive('simpleLoading', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    scope.$watch(attrs.simpleLoading, function(value) {
                        if (value) element.show();
                        else element.hide();
                    });
                }
            };
        });

        cityApp.filter('spaceless',function(){
            return function(input){
                return input.replace(/\s+/g,'_');
            }
        });

        cityApp.controller('CityListController', function($scope, $http){
            $scope.titles = "List of City";
            $scope.currentPage = 1;
            $scope.totalPage = 1;
            $scope.limitPage = 10;
            $scope.showLoading = true;

            $http.get(base_url+'city?page=1&limit='+$scope.limitPage).success(function(data, status){
                $scope.showLoading = false;

                $scope.cities = data.result;
                $scope.totalPage = data.total_page;
                $scope.pager = createPager(1, $scope.totalPage);
            });

            $scope.searchCity = function(){
                $http.get(base_url+'city?q='+$scope.q).success(function(data, status){
                    $scope.cities = data.result;
                });

                $scope.currentPage = 1;
            }

            $scope.deleteCity = function($id){
                var delete_this = confirm('Are you sure want to delete this city?');
                if (delete_this) {
                    
                    $http.delete(base_url+'city/'+$id).success(function(data, status){
                        console.log('delete city success');
                    });

                    console.log('in delete city');
                    window.document.location = '#/';
                }
            }

            $scope.changePage = function(page){
                $scope.showLoading = true;
                $http.get(base_url+'city?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.cities = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });
                $scope.currentPage = page;
            }

            $scope.prevPage = function(){
                if ($scope.currentPage > 1)
                {
                    var page = $scope.currentPage - 1;
                }
                else
                {
                    var page = 1;
                }
                
                console.log(page);

                $scope.showLoading = true;
                $http.get(base_url+'city?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.cities = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });

                $scope.currentPage = page;
            }

            $scope.nextPage = function(){
                if ($scope.currentPage < $scope.totalPage)
                {
                    var page = $scope.currentPage + 1;
                }
                else
                {
                    var page = $scope.totalPage;
                }

                $scope.showLoading = true;
                $http.get(base_url+'city?page='+page+'&limit='+$scope.limitPage).success(function(data, status){
                    $scope.cities = data.result;
                    $scope.showLoading = false;
                    $scope.totalPage = data.total_page;

                    $scope.pager = createPager(page, $scope.totalPage);
                });

                $scope.currentPage = page;
            }

            function createPager(page, total)
            {
                console.log(page);
                        
                var pager_length = 5;
                var pager = "";
                if (page >= pager_length)
                {
                    if (page > total - pager_length && page <= total)
                    {
                        console.log(' page akhir ');
                        pager = '<ul class="pagination">'+
                                   '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                        
                        for (var i = total - pager_length  + 1; i <= total; i ++)
                        {
                          pager += '<li><a href="javascript:void(0);" ng-click="changePage('+ ( i ) +');">'+ ( i ) +'</a></li>';
                        }          
                            
                        if (page < total)
                        {
                            pager +=   '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>';
                        }
                        
                        pager +=   '</ul>';
                    }
                    else
                    {
                        console.log('page tengah');

                        pager = '<ul class="pagination">'+
                                   '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                        
                        for (var i = 0; i < 5; i ++)
                        {
                          pager += '<li><a href="javascript:void(0);" ng-click="changePage('+ (page + i ) +');">'+ (page + i ) +'</a></li>';
                        }          
                            
                        pager +=   '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>'+
                                '</ul>';
                    }
                    
                }
                else if (page < pager_length)
                {
                    console.log('page awal');
                    var prev ="";
                    if (page > 1)
                    {
                        prev = '<li><a href="javascript:void(0);" ng-click="prevPage();">&laquo;</a></li>';
                    
                    }
                   
                    pager = '<ul class="pagination">'+
                              prev+
                              '<li><a href="javascript:void(0);" ng-click="changePage(1);">1</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(2);">2</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(3);">3</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(4);">4</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="changePage(5);">5</a></li>'+
                              '<li><a href="javascript:void(0);" ng-click="nextPage();">&raquo;</a></li>'+
                            '</ul>';
                }
                

                return pager;
            }
        });

        cityApp.controller('CityEditController', function($scope, $http, $routeParams){
            
            $http.get(base_url+'city/'+$routeParams.cityId).success(function(data, status){
                $scope.titles = "Edit City - "+data.Name;
                $scope.city = data;
            });

            $scope.submitTrip = function(){
                $http({
                    method:"PUT",
                    url: base_url+'city/'+$routeParams.cityId, 
                    data: $.param({
                        'name':$scope.city.Name,
                        'country_code':$scope.city.CountryCode,
                        'district':$scope.city.District,
                        'population':$scope.city.Population
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function(data, status){
                    window.document.location = '#/';
                });
            }
        });

        cityApp.controller('CityNewController', function($scope, $http){
            
            $scope.titles = "Add New City";
            $scope.city = {Name:"", CountryCode:"", District:"", Population:""};

            $scope.submitTrip = function(){
                console.log('add trip');;
                $http({
                    method:"POST",
                    url: base_url+'city', 
                    data: $.param({
                        'name':$scope.city.Name,
                        'country_code':$scope.city.CountryCode,
                        'district':$scope.city.District,
                        'population':$scope.city.Population
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function(data, status){
                    window.document.location = '#/';
                });
            }
        });

       
    </script>
</body>

</html>
