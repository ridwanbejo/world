<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<h2>Contact Form</h2>
		<div>Someone just contacted us: </div>
		<ul>
			<li> Subject: {{ $subject }}</li>
			<li> Message: {{ $email_message }}</a></li>
		</ul>
	</body>
</html>