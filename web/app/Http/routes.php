<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'login'], function(){
	Route::get('/dashboard', 'HomeController@dashboard');
	Route::get('/country', 'HomeController@country');
	Route::get('/country_lang', 'HomeController@countryLang');
	Route::get('/city', 'HomeController@city');
	Route::get('/continent', 'HomeController@continent');
	Route::get('/region', 'HomeController@region');
	Route::get('/government_form', 'HomeController@governmentForm');
	Route::get('/head_of_state', 'HomeController@headOfState');
});

Route::get('/login', 'HomeController@login');
Route::post('/login', 'PrimitiveAuthController@doLogin');
Route::get('/logout', 'PrimitiveAuthController@doLogout');
Route::get('/check_session', 'PrimitiveAuthController@checkSession');

Route::get('/partials/{filename}', function($filename){
	return view('world/partials/'.$filename);
});

