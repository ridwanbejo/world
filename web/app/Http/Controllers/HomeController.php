<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller {
	
	public function dashboard(Request $request)
	{
		// $user = new User();
		// if ($user->check($request)) 
		// {
			return view('world/dashboard');
		// }
		// else
		// {
		// 	return redirect('login');
		// }
	}

	public function country(Request $request) 
	{
		$user = new User();
		if ($user->check($request)) 
		{
			return view('world/country');
		}
		else
		{
			return redirect('login');
		}
	}

	public function city(Request $request) 
	{
		$user = new User();
		if ($user->check($request)) 
		{
			return view('world/city');
		}
		else
		{
			return redirect('login');
		}
	}

	public function countryLang(Request $request) 
	{
		$user = new User();
		if ($user->check($request)) 
		{
			return view('world/country_lang');
		}
		else
		{
			return redirect('login');
		}
	}

	public function continent(Request $request) 
	{
		$user = new User();
		if ($user->check($request)) 
		{
			return view('world/continent');
		}
		else
		{
			return redirect('login');
		}
	}

	public function region(Request $request) 
	{
		$user = new User();
		if ($user->check($request)) 
		{
			return view('world/region');
		}
		else
		{
			return redirect('login');
		}
	}

	public function governmentForm(Request $request) 
	{
		$user = new User();
		if ($user->check($request)) 
		{
			return view('world/government_form');
		}
		else
		{
			return redirect('login');
		}
	}

	public function headOfState(Request $request) 
	{
		$user = new User();
		if ($user->check($request)) 
		{
			return view('world/head_of_state');
		}
		else
		{
			return redirect('login');
		}
	}

	public function login ()
	{
		return view('auth/login');
	}
}