<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PrimitiveAuthController extends Controller {

	public function doLogin(Request $request)
	{
		$request->session()->put('username', 'ridwanbejo');
		$request->session()->put('email', 'ridwanbejo@gmail.com');

		return redirect('dashboard');
	}

	public function doLogout(Request $request)
	{
		$request->session()->flush();
		
		return redirect('login');
	}

	public function checkSession(Request $request)
	{
		$data = $request->session()->all();
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}
}