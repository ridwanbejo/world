<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/* BEGIN: Routing World API */

$app->get('country', 'CountryController@getCountries');
$app->get('country/lang/top', 'CountryController@getTopTenByLanguage');
$app->get('country/city/top', 'CountryController@getTopTenByCity');
$app->get('country/population/top', 'CountryController@getTopTenByPopulation');
$app->get('country/surfacearea/top', 'CountryController@getTopTenBySurfaceArea');
$app->get('country/lifeexp/top', 'CountryController@getTopTenByLifeExpectancy');
$app->get('country/gnp/top', 'CountryController@getTopTenByGNP');
$app->get('country/head/top', 'CountryController@getTopTenHeadOfState');
$app->get('country/govt/top', 'CountryController@getTopTenGovt');
$app->get('country/indep_year/top', 'CountryController@getTopTenIndepYear');
$app->get('country/info/{info}', 'CountryController@getCountryInfo');
$app->get('country/{lang}/lang', 'CountryController@getCountryByLanguage');
$app->get('country/{lang}/lang/{area}', 'CountryController@getAreaByLanguage');
$app->get('country/{country_id}', 'CountryController@getCountry');
$app->get('country/{area}/area', 'CountryController@getCountryByArea');
$app->get('country/{govt_name}/govt', 'CountryController@getCountryByGovt');
$app->get('country/continent/{continent}', 'CountryController@getCountryByContinent');
$app->get('country/region/{region}', 'CountryController@getCountryByRegion');
$app->get('country/{field}/{area}', 'CountryController@getFactByArea');
$app->get('country/indep_year/{area_name}/{area}/top', 'CountryController@getTopTenIndepYearByArea');
$app->get('country/govt/{area_name}/{area}/top', 'CountryController@getTopTenGovtFormByArea');
$app->get('country/{field}/{area}/{area_name}/top', 'CountryController@getTopTenByArea');
$app->post('country', 'CountryController@setCountry');
$app->post('country/capital', 'CountryController@setCapital');
$app->put('country/{country_id}', 'CountryController@updateCountry');
$app->delete('country/{country_id}', 'CountryController@deleteCountry');

$app->get('city', 'CityController@getCities');
$app->get('city/population/top', 'CityController@getTopTenByPopulation');
$app->get('city/continent', 'CityController@getCityByContinent');
$app->get('city/region', 'CityController@getCityByRegion');
$app->get('city/{city_id}', 'CityController@getCity');
$app->get('city/{country_id}/country', 'CityController@getCityByCountry');
$app->put('city/{city_id}', 'CityController@updateCity');
$app->post('city', 'CityController@setCity');
$app->delete('city/{city_id}', 'CityController@deleteCity');

$app->get('country_lang', 'CountryLangController@getCountryLang');
$app->get('country_lang/all', 'CountryLangController@getWorldLanguage');
$app->get('country_lang/top', 'CountryLangController@getTopTenLanguage');
$app->get('country_lang/{country_id}/country', 'CountryLangController@getCountryLangByCountry');
$app->get('country_lang/{country_code}/{language}', 'CountryLangController@getDetailCountryLang');
$app->get('country_lang/{area_name}/{area}/top', 'CountryLangController@getTopTenByArea');
$app->post('country_lang', 'CountryLangController@setCountryLang');
$app->put('country_lang/{country_code}/{language}', 'CountryLangController@updateCountryLang');
$app->delete('country_lang/{country_code}/{language}', 'CountryLangController@deleteCountryLang');

/* END: Routing for World API*/
