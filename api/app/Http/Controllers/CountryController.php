<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }
    
    public function getCountries(Request $request)
    {
        $query_limit = "";
        $limit = 1;
        $page = 0;

        if (isset($_GET['page']) && isset($_GET['limit']))
        {
            if ($request->page > 0)
            {
                $page = ( $request->page - 1 ) * $request->limit;
            }
            else
            {
                $page = 0;
            }

            if ($_GET['limit'])
            {
                $limit = $request->limit;
            }
            
        }
   
        $criteria = "";
        if (isset($_GET['continent']))
        {
            $criteria .= ' AND Continent ="'.$request->continent.'" ';            
        }
        
        if (isset($_GET['region']))
        {
            $criteria .= ' AND Region ="'.$request->region.'" ';            
        }

        if (isset($_GET['q']))
        {
            if ($_GET['q'] == "")
            {
                $limit = 10;
                $query_limit = ' LIMIT '.$page.', '.$limit;
            }
            else
            {
                $query_limit = '';
            }
        
            $criteria .= ' AND Name like "%'.$request->q.'%" ';            
        }
        else
        {
            $query_limit = ' LIMIT '.$page.', '.$limit;
        }

        $sorter = "";
        if (isset($_GET['sort_by']))
        {
            $criteria .= ' ORDER BY '.$request->sort_by.' ';            
        }

        
        $query1 = 'SELECT count(Code) as total_rows FROM Country WHERE 1 '.
                                    $criteria.
                                        $sorter;

        $rows =  DB::selectOne($query1);

        $query2 = 'SELECT * FROM Country WHERE 1 '.
                                    $criteria.
                                        $sorter.
                                            $query_limit;

        $results =  DB::select($query2);
        

        $total_page = $rows->total_rows / $limit;

        return response()->json(array('total_row'=> $rows->total_rows, 'total_page'=>$total_page, 'result'=>$results));
    }

    public function getCountry($country_id)
    {
        $results =  DB::selectOne('SELECT * FROM Country WHERE Code = "'.$country_id.'"');

        return response()->json($results);
    }

    public function getCountryInfo(Request $request, $field)
    {
        $criteria = "";
        if (isset($_GET['q']))
        {
            $criteria .= ' AND '.ucwords($field).' like "%'.$request->q.'%" ';            
        }

        $results =  DB::select('SELECT Distinct('.ucwords($field).') as Name FROM Country WHERE 1  '.$criteria);

        return response()->json($results);
    }

    public function getCountryByLanguage(Request $request, $language)
    {

        $language = str_replace("_", " ", $language);
        $results =  DB::select('SELECT c.Name, c.Code, c.Region, c.Continent FROM Country c JOIN CountryLanguage cl on c.Code = cl.CountryCode WHERE cl.Language= ? ', [$language]);

        return response()->json($results);
    }

    public function getCountryByContinent($continent)
    {
        $continent = str_replace("_", " ", $continent);
        $results =  DB::select('SELECT c.Name, c.Code FROM Country c  WHERE c.Continent = ? ', [$continent]);

        return response()->json($results);
    }
    public function getCountryByRegion($region)
    {
        $region = str_replace("_", " ", $region);
        $results =  DB::select('SELECT c.Name, c.Code FROM Country c  WHERE c.Region = ? ', [$region]);

        return response()->json($results);
    }

    public function getAreaByLanguage($area, $language)
    {
        $area = ucwords($area);
        $language = str_replace("_", " ", $language);
        $results =  DB::select('SELECT c.'.$area.' as Name, count(c.'.$area.') as TotalCountries FROM Country c JOIN CountryLanguage cl on c.Code = cl.CountryCode WHERE cl.Language= ? GROUP BY c.'.$area.'', [$language]);

        return response()->json($results);
    }

    public function getCountryByArea(Request $request, $field)
    {
        $results =  DB::select('SELECT '.ucwords($field).' as Name, count(Name) as TotalCountries FROM Country GROUP BY '.ucwords($field));

        return response()->json($results);
    }

    public function getTopTenByLanguage()
    {
        $results =  DB::select('SELECT c.Name, count(cl.Language) as TotalItem FROM Country c JOIN CountryLanguage cl on c.Code = cl.CountryCode GROUP BY c.Name ORDER BY TotalItem DESC LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenByCity()
    {
        $results =  DB::select('SELECT c.Name, count(ct.Name) as TotalItem FROM Country c JOIN City ct on c.Code = ct.CountryCode GROUP BY c.Name ORDER BY TotalItem DESC LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenByGNP()
    {
        $results =  DB::select('SELECT c.Name, c.GNP as GNP FROM Country c ORDER BY GNP DESC LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getFactByArea($field, $area)
    {
        $aggregation = "";
        if ($field == 'LifeExpectancy')
        {
            $aggregation = 'avg(c.'.$field.')';
        }
        else
        {
            $aggregation = 'sum(c.'.$field.')';
        }
        $query = 'SELECT c.'.ucwords($area).' as Name, '.$aggregation.' as '.$field.' FROM Country c GROUP BY '.ucwords($area).' ORDER BY '.$field.' DESC';
        $results =  DB::select($query);
        
        return response()->json($results);
    }

    public function getTopTenByPopulation()
    {
        $results =  DB::select('SELECT c.Name, c.Population FROM Country c ORDER BY Population DESC LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenBySurfaceArea()
    {
        $results =  DB::select('SELECT c.Name, c.SurfaceArea  FROM Country c ORDER BY SurfaceArea DESC LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenByLifeExpectancy()
    {
        $results =  DB::select('SELECT c.Name, c.LifeExpectancy  FROM Country c ORDER BY LifeExpectancy DESC LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenGovt()
    {
        $results =  DB::select('SELECT Name, TotalItem FROM (SELECT c.GovernmentForm as Name, count(c.GovernmentForm) as TotalItem FROM Country c  GROUP BY GovernmentForm ORDER BY TotalItem DESC) gf LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenHeadOfState()
    {
        $results =  DB::select('SELECT Name, TotalItem FROM (SELECT c.HeadOfState as Name, count(c.HeadOfState) as TotalItem FROM Country c GROUP BY HeadOfState ORDER BY TotalItem DESC) hs WHERE TotalItem > 1 and Name <> "" LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenIndepYear()
    {
        $results =  DB::select('SELECT Name, Sovereign FROM (SELECT c.Name, (YEAR(NOW()) - c.IndepYear) as Sovereign FROM Country c where c.IndepYear is not null ORDER BY Sovereign DESC) iy  LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenByArea($field, $area, $area_name)
    {
        $area_name = str_replace("_", " ", $area_name);
        $query = 'SELECT c.Name, '.$field.' as TotalItem FROM Country c Where '.ucwords($area).' = "'.$area_name.'" ORDER BY '.$field.' DESC LIMIT 0, 10';
        $results =  DB::select($query);
        
        return response()->json($results);
    }

    public function getTopTenIndepYearByArea($area_name, $area)
    {
        $area_name = str_replace("_", " ", $area_name);
        $results =  DB::select('SELECT Name, TotalItem FROM (SELECT c.Name, (YEAR(NOW()) - c.IndepYear) as TotalItem FROM Country c where c.IndepYear is not null and c.'.$area.' = "'.$area_name.'" ORDER BY TotalItem DESC) iy  LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getTopTenGovtFormByArea($area_name, $area)
    {
        $area_name = str_replace("_", " ", $area_name);
        $results =  DB::select('SELECT Name, TotalItem FROM (SELECT c.GovernmentForm as Name, count(c.GovernmentForm) as TotalItem FROM Country c  WHERE c.'.$area.' = "'.$area_name.'" GROUP BY GovernmentForm ORDER BY TotalItem DESC) gf LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getCountryByGovt($govt_name)
    {
        $govt = str_replace("_", " ", $govt_name);
        $results =  DB::select('SELECT Name, Region, Continent, Code  FROM Country WHERE GovernmentForm = "'.$govt.'"');

        return response()->json($results);
    }

    public function setCountry(Request $request)
    {
        DB::insert('insert into Country (Code, Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, GNP, GNPOld, LocalName, GovernmentForm, HeadOfState, Capital, Code2) values (?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?)', 
                                    [
                                        $request->Code, 
                                        $request->Name, 
                                        $request->Continent, 
                                        $request->Region, 
                                        $request->SurfaceArea,
                                        $request->IndepYear,
                                        $request->Population,
                                        $request->LifeExpectancy,
                                        $request->GNP,
                                        0.0,
                                        $request->LocalName,
                                        $request->GovernmentForm,
                                        $request->HeadOfState,
                                        NULL,
                                        $request->Code2
                                    ]
                                );

        $results = array('msg'=>'Add country success');
        return response()->json($results);
    }

    public function updateCountry(Request $request, $country_id)
    {
        DB::update('update Country set Name = ?, Continent = ?, Region = ?, SurfaceArea = ?, IndepYear = ?, Population = ?, LifeExpectancy = ?, GNP = ?, LocalName = ?, GovernmentForm = ?, HeadOfState = ?, Code2 =? WHERE Code = ?', 
                                    [
                                        $request->Name, 
                                        $request->Continent, 
                                        $request->Region, 
                                        $request->SurfaceArea,
                                        $request->IndepYear,
                                        $request->Population,
                                        $request->LifeExpectancy,
                                        $request->GNP,
                                        $request->LocalName,
                                        $request->GovernmentForm,
                                        $request->HeadOfState,
                                        $request->Code2,
                                        $request->Code, 
                                    ]
                                );

        $results = array('msg'=>'Update country success');
        return response()->json($results);
    }

    public function setCapital(Request $request)
    {
        DB::update('update Country  set Capital = ? where Code = ?', 
                                    [
                                        $request->city_id,
                                        $request->Code, 
                                    ]
                                );

        $results = array('msg'=>'Add country success', 'req'=>$request);
        return response()->json($results);
    }

    public function deleteCountry($country_id)
    {
        $results =  DB::delete('DELETE FROM Country WHERE Code = "'.$country_id.'"');

        $results = array('msg'=>'Delete country success');
        return response()->json($results);
    }
}
