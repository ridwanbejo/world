<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CountryLangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }
    
    public function getCountryLang(Request $request)
    {
        $query_limit = "";
        if (isset($_GET['page']) && isset($_GET['limit']))
        {
            $page = $request->page;
            $limit = $request->limit;
            $query_limit = ' LIMIT '.$page.', '.$limit;
        }

        $criteria = "";
        if (isset($_GET['lang']))
        {
            $criteria .= ' AND Language ="'.$request->lang.'" ';            
        }

        if (isset($_GET['official']))
        {
            $criteria .= ' AND IsOfficial ="'.$request->official.'" ';            
        }

        $results =  DB::select('SELECT * from CountryLanguage WHERE 1 '.$criteria.$query_limit);

        return response()->json($results);
    }

    public function getDetailCountryLang($country_code, $lang)
    {
        $results =  DB::selectOne('SELECT * from CountryLanguage WHERE CountryCode = ? and Language = ? ', 
                                    [
                                        $country_code,
                                        $lang
                                    ]
                                );

        return response()->json($results);
    }

    public function getCountryLangByCountry(Request $request, $country_code)
    {
        $query_limit = "";
        if (isset($_GET['page']) && isset($_GET['limit']))
        {
            $page = $request->page;
            $limit = $request->limit;
            $query_limit = ' LIMIT '.$page.', '.$limit;
        }

        $criteria = "";
        if (isset($_GET['official']))
        {
            $criteria .= ' AND IsOfficial ="'.$request->official.'" ';            
        }

        $sorter = "";
        if (isset($_GET['sort_by']))
        {
            $criteria .= ' ORDER BY '.$request->sort_by.' ';            
        }

        $results =  DB::select('SELECT * from CountryLanguage WHERE CountryCode = "'.$country_code.'" '.
                                $criteria.
                                    $sorter.
                                        $query_limit
                    );

        return response()->json($results);
    }

    public function getWorldLanguage(Request $request)
    {
        $query_limit = "";
        $limit = 1;
        $page = 0;

        if (isset($_GET['page']) && isset($_GET['limit']))
        {
            if ($request->page > 0)
            {
                $page = ( $request->page - 1 ) * $request->limit;
            }
            else
            {
                $page = 0;
            }

            if ($_GET['limit'])
            {
                $limit = $request->limit;
            }
            
        }

        $criteria = "";
        if (isset($_GET['q']))
        {
            if ($_GET['q'] == "")
            {
                $limit = 10;
                $query_limit = ' LIMIT '.$page.', '.$limit;
            }
            else
            {
                $query_limit = '';
            }
        
            $criteria .= ' AND Language like "%'.$request->q.'%" ';            
        }
        else
        {
            $query_limit = ' LIMIT '.$page.', '.$limit;
        }

        $query1 = 'SELECT count(Language) as total_rows FROM (SELECT Language, count(Language) as "TotalUsed" FROM CountryLanguage WHERE 1 '.$criteria.' GROUP BY Language ORDER BY TotalUsed DESC) wl ';
        $rows =  DB::selectOne($query1);
        
        $query2 = 'SELECT Language, TotalUsed FROM (SELECT Language, count(Language) as "TotalUsed" FROM CountryLanguage WHERE 1 '.$criteria.' GROUP BY Language ORDER BY TotalUsed DESC) wl '.$query_limit ;
        $results =  DB::select($query2);

        $total_page = round($rows->total_rows / $limit);

        return response()->json(array('total_row'=> $rows->total_rows, 'total_page'=>$total_page, 'result'=>$results));
    }

    public function getTopTenLanguage()
    {
        $results =  DB::select('SELECT Name, TotalItem FROM (SELECT  Language as Name, count(Language) as TotalItem FROM CountryLanguage GROUP BY Language ORDER BY TotalItem DESC) l LIMIT 0, 10');

        return response()->json($results);
    }

    public function getTopTenByArea($area_name, $area)
    {
        $area_name = str_replace("_", " ", $area_name);
        $results =  DB::select('SELECT Name, TotalItem FROM (SELECT  cl.Language as Name, count(cl.Language) as TotalItem FROM CountryLanguage cl JOIN Country c on cl.CountryCode = c.Code WHERE c.'.ucwords($area).' = "'.$area_name.'" GROUP BY cl.Language ORDER BY TotalItem DESC) l LIMIT 0, 10');

        return response()->json($results);
    }

    public function setCountryLang(Request $request)
    {
        DB::insert('insert into CountryLanguage (CountryCode, Language, IsOfficial, Percentage) values (?, ?, ?, ?)', 
                                    [
                                        $request->country_code, 
                                        $request->name, 
                                        $request->is_official, 
                                        $request->percentage, 
                                    ]
                                );
        
        $results = array('msg'=>'Add country language is success');
        return response()->json($results);
    }

    public function updateCountryLang(Request $request, $country_code, $language)
    {
        $deleted = DB::delete('update CountryLanguage set IsOfficial = ?, Percentage = ? where CountryCode = ? and Language = ? ', 
                                    [
                                        $request->is_official,
                                        $request->percentage,
                                        $country_code,
                                        $language
                                    ]
                                );
        
        $results = array('msg'=>'Update country language is success');
        return response()->json($results);
    }

    public function deleteCountryLang($country_code, $language)
    {
        $deleted = DB::delete('delete from CountryLanguage where CountryCode = ? and Language = ? ', 
                                    [
                                        $country_code,
                                        $language
                                    ]
                                );
        
        $results = array('msg'=>'Delete city is success');
        return response()->json($results);
    }
}
