<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }
    
    public function getCities(Request $request)
    {
        $query_limit = "";
        $limit = 1;
        $page = 0;

        if (isset($_GET['page']) && isset($_GET['limit']))
        {
            if ($request->page > 0)
            {
                $page = ( $request->page - 1 ) * $request->limit;
            }
            else
            {
                $page = 0;
            }

            if ($_GET['limit'])
            {
                $limit = $request->limit;
            }
            
        }

        $criteria = "";
        
        if (isset($_GET['q']))
        {
            if ($_GET['q'] == "")
            {
                $limit = 10;
                $query_limit = ' LIMIT '.$page.', '.$limit;
            }
            else
            {
                $query_limit = '';
            }
        
            $criteria .= ' AND Name like "%'.$request->q.'%" ';            
        }
        else
        {
            $query_limit = ' LIMIT '.$page.', '.$limit;
        }

        $query1 = 'select count(Name) as total_rows from City WHERE 1 '.$criteria;
        $rows =  DB::selectOne($query1);

        $query2 = 'select * from City WHERE 1 '.$criteria.$query_limit;
        $results =  DB::select($query2);
        
        $total_page = round($rows->total_rows / $limit);

        return response()->json(array('total_row'=> $rows->total_rows, 'total_page'=>$total_page, 'result'=>$results));
    }

    public function getCity($city_id)
    {
        $results =  DB::selectOne('select * from City where ID = '.$city_id);
        return response()->json($results);
    }

    public function getCityByCountry(Request $request, $country_code)
    {
        $query_limit = "";
        if (isset($_GET['page']) && isset($_GET['limit']))
        {
            $page = $request->page;
            $limit = $request->limit;
            $query_limit = ' LIMIT '.$page.', '.$limit;
        }

        $criteria = "";
        if (isset($_GET['district']))
        {
            $criteria .= ' AND District ="'.$request->district.'" ';            
        }

        $sorter = "";
        if (isset($_GET['sort_by']))
        {
            $criteria .= ' ORDER BY '.$request->sort_by.' ';            
        }

        $results =  DB::select('select * from City where CountryCode= "'.$country_code.'" '.
                                    $criteria.
                                        $sorter.
                                            $query_limit
                            );
        
        return response()->json($results);
    }

    public function getTopTenByPopulation()
    {
        $results =  DB::select('SELECT c.Name, c.Population as Population FROM City c ORDER BY Population DESC LIMIT 0, 10');
        
        return response()->json($results);
    }

    public function getCityByContinent()
    {
        $results =  DB::select('SELECT c.Continent as Name, count(ct.Name) as TotalItem FROM Country c JOIN City ct GROUP BY c.Continent ORDER BY TotalItem DESC');
        
        return response()->json($results);
    }

    public function getCityByRegion()
    {
        $results =  DB::select('SELECT c.Region as Name, count(ct.Name) as TotalItem FROM Country c JOIN City ct GROUP BY c.Region ORDER BY TotalItem DESC');
        
        return response()->json($results);
    }

    public function updateCity(Request $request, $city_id)
    {
        $affected = DB::update('update City set Name = ?, CountryCode = ?, District = ?, Population = ? where ID = ?', 
                                    [
                                        $request->name, 
                                        $request->country_code, 
                                        $request->district, 
                                        $request->population, 
                                        $city_id
                                    ]
                                );

        $results = array('msg'=>'Edit city is success');
        return response()->json($results);
    }

    public function setCity(Request $request)
    {
        $id = DB::insert('insert into City (Name, CountryCode, District, Population) values (?, ?, ?, ?)', 
                                    [
                                        $request->name, 
                                        $request->country_code, 
                                        $request->district, 
                                        $request->population, 
                                    ]
                                );
        
        $results = array('msg'=>'Add city is success', 'city_id'=>$id);
        return response()->json($results);
    }

    public function deleteCity(Request $request, $city_id)
    {
        $deleted = DB::delete('delete from City where ID = ? ', 
                                    [
                                        $city_id
                                    ]
                                );
        
        $results = array('msg'=>'Delete city is success');
        return response()->json($results);
    }
}
