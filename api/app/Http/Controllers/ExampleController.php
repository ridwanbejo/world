<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function hello()
    {
        return "Hello World :D...";
    }

    public function get_country()
    {
        $results =  DB::select('select *, md5(country_id) as country_id from country');
        return response()->json($results);
    }
}
